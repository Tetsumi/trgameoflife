trgameoflife
===================

John Conway's Game of Life written in Racket/gui.

Left mouse: Turn a cell alive.  
Right mouse: Kill a cell.

![Screenshot](http://i.imgur.com/GIP5GiZ.png)

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png) 

[Note: I am no longer working on this.]